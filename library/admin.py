from django.contrib import admin
from .models import Shelves, Books
# Register your models here.

admin.site.register(Shelves)
admin.site.register(Books)
