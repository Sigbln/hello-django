from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from .models import Shelves, Books


# Create your views here.

class LibraryView(TemplateView):
    """Список полок"""

    def get(self, request):
        shelves = Shelves.objects.all()
        books = Books.objects.all()
        return render(request, "./home.html",
                      {"shelve_list": shelves, "book_list": books})


class ShelveView(TemplateView):
    """Список книг на полке"""

    def get(self, request, slug):
        books = Books.objects.filter(shelve=slug)
        return render(request, "./shalve.html", {"book_list": books})


class GetBook(TemplateView):
    """Взять книгу"""

    def post(self, request, pk):
        book = Books.objects.get(id=pk)
        if book.status is True:
            book.status = False
            book.save()
        return redirect(f'/{book.shelve}')


class PutBook(TemplateView):
    """Взять книгу"""

    def post(self, request, pk):
        book = Books.objects.get(id=pk)
        if book.status is False:
            book.status = True
            book.save()
        return redirect(f'/{book.shelve}')
